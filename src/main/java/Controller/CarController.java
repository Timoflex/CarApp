package Controller;

import Model.CarModel;
import Service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;

    @PostMapping("addCar")
    public void add(CarModel model) {
        carService.add(model);
    }

//    @DeleteMapping
//    public void remove(Integer id) {
//    carService.remove(id);
//    }

    @PatchMapping("editCar")
    public void edit() {

    }

    @GetMapping("getCarList")
    public void getList() {
        carService.getList();
    }
}