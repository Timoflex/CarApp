package Repository;

import Model.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface CarRepos extends JpaRepository<CarModel, Integer>,
        JpaSpecificationExecutor<CarModel>
    {
        CarModel save(CarModel model);
        List<CarModel> findAll();
        Optional<CarModel> findById(Integer id);
    }

