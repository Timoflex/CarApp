package Model;

import lombok.Data;

@Data
public class CarModel {

    private String CarBrand;
    private Integer CarNumber;

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String carBrand) {
        CarBrand = carBrand;
    }

    public Integer getCarNumber() {
        return CarNumber;
    }

    public void setCarNumber(Integer carNumber) {
        CarNumber = carNumber;
    }
}
