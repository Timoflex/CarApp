package Service;

import Model.CarModel;
import Repository.CarRepos;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {
    private final CarRepos carRepos;

    public CarService(CarRepos carRepos) {
        this.carRepos = carRepos;
    }

    public void add(CarModel model) {
        carRepos.save(model);
    }

    public List<CarModel> getList() {
        return carRepos.findAll();
    }

//    public Integer remove(Integer id) { return carRepos.deleteById(id);
//    }
}
